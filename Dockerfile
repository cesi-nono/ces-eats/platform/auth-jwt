FROM node:latest
EXPOSE 3000

WORKDIR /app

COPY . .

RUN npm install
RUN npm install express
RUN npm install dotenv
RUN npm install supervisor -g

CMD [ "supervisor", "app.js" ]